# frozen_string_literal: true

require 'erb'

module CRB

  class Room

    DEFAULT_TEMPLATE_DIR = File.expand_path(File.join(['..'] * 2, 'data'), __FILE__)
    DEFAULT_TEMPLATE = 'room_voices.orc.erb'
    DEFAULT_ROOM_INSTR_OFFSET = 300

    attr_reader :conf, :template_filename, :template, :instr_offset

    #
    # +CRB::Room+
    #
    # takes one argument: a Configuration with the needed data
    #
    # It uses a template to create the room instrument with the configured
    # speakers, virtual room dimensions etc.
    #
    def initialize(h, tf = DEFAULT_TEMPLATE, io = DEFAULT_ROOM_INSTR_OFFSET)
      @conf = h
      @template_filename = File.join(DEFAULT_TEMPLATE_DIR, tf)
      @template = load_template
      @instr_offset = io
    end

    def run
      return self.template.result(get_binding)
    end
    
    def instrs
      ipl = 10 # number of instruments per line
      result = ''
      idx = 0
      start = self.instr_offset
      stop  = start + self.conf.num_instr - 1
      start.upto(stop) do
        |ni|
        result += ni.to_s
        idx += 1
        result += ', ' if idx < self.conf.num_instr
        result += "\\\n        " if ((idx % ipl) == 0 && idx < self.conf.num_instr)
      end
      return result
    end

    include CRB::ClassMethods

    def output_stage
      result = [ "\t;\n\t; OUTPUT STAGE\n\t;" ]
      self.conf.speakers.each do
        |tag, sp|
        result << sp.output(self.conf.room.walls.values)
      end
      outchnls = "\n\n\toutc "
      last_tag = self.conf.speakers.keys[-1]
      self.conf.speakers.each do
        |tag, sp|
        outchnls += "#{s('a%ssig' % sp.mangled_tag)}"
        outchnls += ", " unless tag == last_tag
      end
      result << f(outchnls, "output everything")
      return result.join("\n")
    end

    def reverb_initialization
      result = []
      self.conf.speakers.each do
        |tag, sp|
        result << f("\t#{s('garev%s' % tag)} init 0", "reverb send initialization for channel %s" % tag)
      end
      return result.join("\n")
    end

    def reverb_send
      result = []
      self.conf.speakers.each do
        |tag, sp|
        gtag = 'garev%s' % tag
        result << f("\t#{s(gtag)} = #{s(gtag)}+iattarev*(1/sqrt(#{s('kdist%s' % sp.mangled_tag)}))", "reverb send for channel %s" % tag)
      end
      return result.join("\n")
    end

    def reverb_receive
      result = []
      self.conf.speakers.each do
        |tag, sp|
        gtag = 'garev%s' % tag
        ars  = 'ars%s' % tag
        arev = 'arev%s' % tag
        result << f("\t#{s(ars)} butterlp #{s(gtag)}, 4000", "hi-freq roll off on reverb send for channel %s" % tag)
        result << f("\t#{s(arev)} nreverb #{s(ars)}, irevt+idrevt, iatf", "actual reverberation for channel %s" % tag)
      end
      return result.join("\n")
    end

    def reverb_output
      outchnls = "\n\n\toutc "
      last_tag = self.conf.speakers.keys[-1]
      self.conf.speakers.each do
        |tag, sp|
        outchnls += "#{s('arev%s' % tag)}"
        outchnls += ", " unless tag == last_tag
      end
      result = f(outchnls, "output reverb")
      return result
    end

    def reverb_clear
      result = []
      self.conf.speakers.each do
        |tag, sp|
        result << f("\t#{s('garev%s' % tag)} = 0", "clear reverb send for channel %s after usage" % tag)
      end
      return result.join("\n")
    end
  private
  
    def get_binding
      binding
    end

    def load_template
      return ERB.new(File.open(self.template_filename, 'r').readlines.join, trim_mode: '-')
    end

  end

end
