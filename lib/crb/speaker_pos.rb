# frozen_string_literal: true

module CRB

  class SpeakerPos < Coord

    attr_reader :tag

    def initialize(h)
      raise Errors::CoordArgumentError, "Coord argument must be an hash" unless h.is_a?(Hash)
      @tag = h.keys.first
      super(h.values.first)
    end

    include CRB::ClassMethods

    def speaker_coordinates
      result = [ f("\t#{s('i%s' % mangled_tag('x'))} = #{'%+9.3f' % self.x}", "spkr %s x coord" % self.tag) ]
      result << f("\t#{s('i%s' % mangled_tag('y'))} = #{'%+9.3f' % self.y}", "spkr %s y coord" % self.tag)
      return result.join("\n")
    end

    def signal_geometry
      result =   [ "\t;\n\t; speaker %s\n\t;" % [self.tag] ]
      result <<  f("\t#{s('k%sdist' % mangled_tag('x'))} = #{s('kx')} - #{s('i%s' % mangled_tag('x'))}", "x dist source -> %s" % mangled_tag('x'))
      result <<  f("\t#{s('k%sdist' % mangled_tag('y'))} = #{s('ky')} - #{s('i%s' % mangled_tag('y'))}", "y dist source -> %s" % mangled_tag('y'))
      result <<  f("\t#{s('k%sdistq' % mangled_tag('x'))} = #{s('k%sdist' % mangled_tag('x'))} * #{s('k%sdist' % mangled_tag('x'))}", "x dist squared")
      result <<  f("\t#{s('k%sdistq' % mangled_tag('y'))} = #{s('k%sdist' % mangled_tag('y'))} * #{s('k%sdist' % mangled_tag('y'))}", "y dist squared")
      result <<  f("\t#{s('kdist%s' % mangled_tag)} = sqrt(#{s('k%sdistq' % mangled_tag('x'))} + #{s('k%sdistq' % mangled_tag('y'))})", "dist dir > speaker %s" % self.tag)
      result <<  f("\t#{s('kdel%s' % mangled_tag)}   port  #{s('kdist%s' % mangled_tag)}/givel,0.1", "delay value signal -> speaker %s" % self.tag)
      result <<  f("\t#{s('a%sdirsig' % mangled_tag)}  deltapi #{s('kdel%s' % mangled_tag)}", "actual delayed direct signal for speaker %s" % self.tag)
      result <<  f("\t#{s('k%sdiramp' % mangled_tag)}  = 1/#{s('kdist%s' % mangled_tag)}", "direct signal attenuation for speaker %s" % self.tag)
      result <<  f("\t#{s('a%sdirsig' % mangled_tag)}  = #{s('a%sdirsig' % mangled_tag)} * #{s('k%sdiramp' % mangled_tag)}", "attenuated (1/d) direct signal for speaker %s" % self.tag)
      return result.join("\n")
    end

    def reflection_geometry(walls)
      result =   [ "\t;\n\t; speaker %s reflections\n\t;" % self.tag ]
      walls.each { |w| result.concat(w.reflection_distance_calculation(self)) }
      return result.join("\n")
    end

    def output(walls)
      result = "\t#{s('a%ssig' % mangled_tag)} = #{s('a%sdirsig' % mangled_tag)} + "
      walls.each do
        |w|
        result += "#{s('a%s%ssig' % [w.mangle, mangled_tag])}"
        result += " + " if w.tag != '4'
      end
      return f(result, "master speaker %s output" % mangled_tag)
    end

    def mangled_tag(coord = '')
      stag = self.tag.is_a?(Numeric) ? sprintf("%02d", self.tag) : self.tag
      return sprintf("spkr%s%c", stag, coord)
    end

  end

end
