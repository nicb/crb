# frozen_string_literal: true

require 'yaml'

module CRB

  module Errors

    class ConfigFileNotFound < Error; end

  end

  class ConfigurationFile

    attr_reader :file_name, :confs

    #
    # +CRB::ConfigurationFile+
    #
    # takes one argument: a string containing the file path
    #
    def initialize(f)
      raise Errors::ConfigFileNotFound, "File #{f} not found" unless File.exist?(f)
      load_configurations(f)
    end

    def conf(tag)
      result = nil
      self.confs.each do
        |conf|
        if tag == conf.tag
          result = conf
          break
        end
      end
      return result
    end

  private

    def load_configurations(f)
      @confs = []
      all_confs = YAML.load(File.open(f, 'r')) 
      all_confs.each { |key, conf| @confs << Configuration.new(key => conf) }
    end

  end

end
