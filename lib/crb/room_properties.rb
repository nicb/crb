# frozen_string_literal: true

module CRB

  class RoomProperties

    attr_reader :width, :depth, :front_left, :front_right, :rear_left, :rear_right, :walls

    def initialize(h)
      raise Errors::CoordArgumentError, "Room size argument must be an hash" unless h.is_a?(Hash)
      configure(h)
    end

    include CRB::ClassMethods

    def outer_room_geometry
      result = []
      result << f("\t#{s('ixroomright')} = %+5.2f" % self.front_right.x, "right side")
      result << f("\t#{s('ixroomleft')}  = %+5.2f" % self.front_left.x,  "left  side")
      result << f("\t#{s('iyroomfront')} = %+5.2f" % self.front_left.y,  "front side")
      result << f("\t#{s('iyroomrear')}  = %+5.2f" % self.rear_left.y,   "rear  side")
      return result.join("\n")
    end

    def virtual_room_distances
      result =  []
      result << f("\t#{s('kxroomright')} = 2*(#{s('ixroomright')}-#{s('kx')})", "reflected front wall source distance")
      result << f("\t#{s('kyroomfront')} = 2*(#{s('iyroomfront')}-#{s('ky')})", "reflected rear  wall source distance")
      result << f("\t#{s('kxroomleft')}  = 2*(#{s('ixroomleft')}-#{s('kx')})",  "reflected left  wall source distance")
      result << f("\t#{s('kyroomrear')}  = 2*(#{s('iyroomrear')}-#{s('ky')})",  "reflected right wall source distance")
      return result.join("\n")
    end

  private

    def configure(h)
      @width = h['width']
      @depth = h['depth']
      @front_left  = Coord.new([-(self.width/2.0), (self.depth/2.0)])
      @front_right = Coord.new([ (self.width/2.0), (self.depth/2.0)])
      @rear_left   = Coord.new([-(self.width/2.0),-(self.depth/2.0)])
      @rear_right  = Coord.new([ (self.width/2.0),-(self.depth/2.0)])
      @walls = {}
      key = '1'
      @walls[key] = FrontWall.new(key => [@front_left,   @front_right])
      key = '2'
      @walls[key] = RightWall.new(key => [@front_right,  @rear_right])
      key = '3'
      @walls[key] = RearWall.new(key => [@rear_right,  @rear_left])
      key = '4'
      @walls[key] = LeftWall.new(key => [@rear_right,  @rear_left])
    end

  end

end
