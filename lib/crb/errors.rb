# frozen_string_literal: true

#
# +CRB::Errors+:
# This is the module for all +CRB+ exceptions.
#
module CRB
  module Errors
    #
    # +CRB::Errors+
    #
    # contains all the exceptions that can be raised within +CRB+
    #
    class Error < ::StandardError
    end

    #
    # +CRB::Errors::NotImplemented+
    #
    # this error gets called on not-yet-implemented methods
    # It takes two arguments:
    # - the caller class
    # - the offending method
    #
    class NotImplemented < Error
      def initialize(klass, method)
        super("#{klass}###{method} not implemented")
      end
    end

    class PureVirtualMetodCalled < Error; end

  end
end
