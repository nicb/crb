# frozen_string_literal: true

module CRB

  module Errors

    class ConfigArgumentError < ::ArgumentError; end

    class BadConfigError < Error; end

  end

  class Configuration

    attr_reader :conf, :tag, :speakers, :room, :num_instr, :refs

    MANDATORY_KEYS = ['speakers', 'room', 'num_instr', 'refs' ]
    #
    # +CRB::Configuration+
    #
    # takes one argument: a hash with the needed data
    # - speaker positions (inner room)
    # - outer (virtual) room dimensions
    #
    def initialize(h)
      raise Errors::ConfigArgumentError, "Config argument must be a hash" unless h.is_a?(Hash)
      load_configuration(h)
    end

  private

    def load_configuration(h)
      @tag = h.keys.first
      @conf = h[tag]
      MANDATORY_KEYS.each { |k| raise Errors::BadConfigError, "Config argument: mandatory key #{k} missing" unless self.conf.has_key?(k) }
      @speakers = {}
      self.conf['speakers'].each do
        |sp|
        tag = sp[0].to_s
        args = { tag => sp[1] }
        self.speakers[tag] = SpeakerPos.new(args)
      end
      @room = RoomProperties.new(self.conf['room'])
      @num_instr = self.conf['num_instr']
      @refs = []
      self.conf['refs'] { |r| self.refs << Ref.new(r) }
    end

  end

end

