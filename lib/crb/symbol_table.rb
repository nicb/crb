# frozen_string_literal: true

require 'singleton'

module CRB

  class SymbolTable < Hash

    include Singleton

    DEFAULT_ST_LOG_FILE = 'room.syms'

    def report
      result =  [ header ]
      result << ("Number of terms: %d\nDetails:" % self.keys.size)
      syms = []
      self.each do
        |key, value|
        tstar  = value == 1 ? 'time' : 'times'
        syms << ("%-30s used %3d %s" % [key + ':', value, tstar])
      end
      sorted_syms = syms.sort { |a, b| a <=> b }
      result.concat(sorted_syms)
      result << trailer
      return result.join("\n")
    end

  private

    def header
      return "CRB::SymbolTable"
    end

    def trailer
      return "End of " + header
    end

  end

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods

    def s(term)
      st = SymbolTable.instance
      if st.has_key?(term)
        st[term] += 1
      else
        st[term]  = 1
      end
      return term
    end

    def f(left_side, comments = '')
      return "%-70s ; %s" % [left_side, comments]
    end

    def sym_log(filename = CRB::SymbolTable::DEFAULT_ST_LOG_FILE)
      st = CRB::SymbolTable.instance
      File.open(filename, 'w') { |fh| fh.puts(st.report) } 
    end

  end

end
