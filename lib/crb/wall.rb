# frozen_string_literal: true

module CRB

  class Wall
    
    attr_reader :tag, :coords

    #
    # +Crb::Wall+
    #
    # a wall is an abstract class characterized by
    # its coordinates
    #
    # The arguments are:
    # - a hash containing all cordinates
    #
    def initialize(h)
      @tag = h.keys.first
      values = h.values.first
      configure(values)
    end

    def start
      return self.coords[0]
    end

    def stop
      return self.coords[1]
    end

    def mangle
      return "w%s" % [ self.tag ]
    end

    def reflection_distance_calculation(s)
      raise PureVirtualMethodCalled, "Method reflection_distance_calculation cannot be called on base CRB:Wall class"
    end

  private

    def configure(v)
      @coords = [ v[0], v[1] ]
    end

  protected

    def reflection_distance_calculation_header(sp, c)
      return "\t;\n\t; refl %s > %s\n\t;" % [self.mangle, sp.mangled_tag(c) ]
    end

  end

  class FrontWall < Wall

    include CRB::ClassMethods

    #
    # +CRB::FrontWall::reflection_distance_calculation
    #
    # - x dimension remains unmodified
    # - sum the double of the distance of source from the front wall (mirror the wall)
    #
    def reflection_distance_calculation(sp)
      les = "%s%s" % [ mangle, sp.mangled_tag ]
      kles = 'k' + les
      ales = 'a' + les
      klesy = kles + 'y'
      result = [ reflection_distance_calculation_header(sp, 'y') ]
      result << f("\t#{s(klesy)} = #{s('k%s' % 'y')} + #{s('kyroomfront')}", "reflected front y coord")
      result << f("\t#{s('%sdist' % kles)} = sqrt(#{s('k%sdistq' % sp.mangled_tag('x'))} + (#{s(klesy)} * #{s(klesy)}))", "reflected front wall source distance")
      result << f("\t#{s('%sdel' % kles)} port #{s('%sdist' % kles)}/givel, 0.1", "reflected front wall source delay")
      result << f("\t#{s('%ssig' % ales)} deltapi #{s('%sdel' % kles)}", "delayed signal reflected from front wall")
      result << f("\t#{s('%samp' % kles)} = 1/#{s('%sdist' % kles)}", "amplitude attenuation for front wall reflection")
      result << f("\t#{s('%ssig' % ales)} = #{s('%ssig' % ales)} * #{s('%samp' % kles)}", "delayed attenuated signal reflected from front wall")
      return result
    end

  end

  class RearWall < Wall

    include CRB::ClassMethods

    #
    # +CRB::RearWall::reflection_distance_calculation
    #
    # - x dimension remains unmodified
    # - subtract the double of the distance of source from the rear wall (mirror the wall)
    # - filter the signal to enhance the 'back' sensation
    #
    def reflection_distance_calculation(sp)
      les = "%s%s" % [ mangle, sp.mangled_tag ]
      kles = 'k' + les
      ales = 'a' + les
      klesy = kles + 'y'
      result = [ reflection_distance_calculation_header(sp, 'y') ]
      result << f("\t#{s(klesy)} = #{s('k%s' % 'y')} - #{s('kyroomrear')}", "reflected rear y coord")
      result << f("\t#{s('%sdist' % kles)} = sqrt(#{s('k%sdistq' % sp.mangled_tag('x'))} + (#{s(klesy)} * #{s(klesy)}))", "reflected rear wall source distance")
      result << f("\t#{s('%sdel' % kles)} port #{s('%sdist' % kles)}/givel, 0.1", "reflected rear wall source delay")
      result << f("\t#{s('%ssig' % ales)} deltapi #{s('%sdel' % kles)}", "delayed signal reflected from rear wall")
      result << f("\t#{s('%samp' % kles)} = 1/#{s('%sdist' % kles)}", "amplitude attenuation for rear wall reflection")
      result << f("\t#{s('%ssig' % ales)} tone #{s('%ssig' % ales)},(20000/(1+abs(#{s('iyroomrear')})))", "filtered signal reflected from rear wall")
      result << f("\t#{s('%ssig' % ales)} = #{s('%ssig' % ales)} * #{s('%samp' % kles)}", "delayed attenuated signal reflected from rear wall")
      return result
    end

  end

  class RightWall < Wall

    include CRB::ClassMethods

    #
    # +CRB::RightWall::reflection_distance_calculation
    #
    # - y dimension remains unmodified
    # - sum the double of the distance of source from the right wall (mirror the wall)
    #
    def reflection_distance_calculation(sp)
      les = "%s%s" % [ mangle, sp.mangled_tag ]
      kles = 'k' + les
      ales = 'a' + les
      klesx = kles + 'x'
      result = [ reflection_distance_calculation_header(sp, 'x') ]
      result << f("\t#{s(klesx)} = #{s('k%s' % 'x')} + #{s('kxroomright')}", "reflected right x coord")
      result << f("\t#{s('%sdist' % kles)} = sqrt(#{s('k%sdistq' % sp.mangled_tag('y'))} + (#{s(klesx)} * #{s(klesx)}))", "reflected right wall source distance")
      result << f("\t#{s('%sdel' % kles)} port #{s('%sdist' % kles)}/givel, 0.1", "reflected right wall source delay")
      result << f("\t#{s('%ssig' % ales)} deltapi #{s('%sdel' % kles)}", "delayed signal reflected from right wall")
      result << f("\t#{s('%samp' % kles)} = 1/#{s('%sdist' % kles)}", "amplitude attenuation for right wall reflection")
      result << f("\t#{s('%ssig' % ales)} = #{s('%ssig' % ales)} * #{s('%samp' % kles)}", "delayed attenuated signal reflected from right wall")
      return result
    end

  end

  class LeftWall < Wall

    include CRB::ClassMethods

    #
    # +CRB::LeftWall::reflection_distance_calculation
    #
    # - y dimension remains unmodified
    # - subtract the double of the distance of source from the left wall (mirror the wall)
    #
    def reflection_distance_calculation(sp)
      les = "%s%s" % [ mangle, sp.mangled_tag ]
      kles = 'k' + les
      ales = 'a' + les
      klesx = kles + 'x'
      result = [ reflection_distance_calculation_header(sp, 'x') ]
      result << f("\t#{s(klesx)} = #{s('k%s' % 'x')} - #{s('kxroomleft')}", "reflected left x coord")
      result << f("\t#{s('%sdist' % kles)} = sqrt(#{s('k%sdistq' % sp.mangled_tag('y'))} + (#{s(klesx)} * #{s(klesx)}))", "reflected left wall source distance")
      result << f("\t#{s('%sdel' % kles)} port #{s('%sdist' % kles)}/givel, 0.1", "reflected left wall source delay")
      result << f("\t#{s('%ssig' % ales)} deltapi #{s('%sdel' % kles)}", "delayed signal reflected from left wall")
      result << f("\t#{s('%samp' % kles)} = 1/#{s('%sdist' % kles)}", "amplitude attenuation for left wall reflection")
      result << f("\t#{s('%ssig' % ales)} = #{s('%ssig' % ales)} * #{s('%samp' % kles)}", "delayed attenuated signal reflected from left wall")
      return result
    end

  end

end
