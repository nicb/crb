# frozen_string_literal: true

module CRB

  module Errors

    class CoordArgumentError < ::ArgumentError; end

  end

  class Coord

    attr_reader :x, :y

    #
    # +CRB::Coord+
    #
    # takes one argument: a two-location coordinate array
    #
    def initialize(a)
      raise Errors::CoordArgumentError, "Coord argument must be an array" unless a.is_a?(Array)
      raise Errors::CoordArgumentError, "Coord argument array must have exactly two elements" unless a.size == 2
      @x = a[0]
      @y = a[1]
    end

  end

  #
  # +CRB::Ref+
  #
  #  reference input position (for debugging)
  #
  class Ref < Coord; end

end
