# frozen_string_literal: true

module CRB
  %w[
    version
    errors
    symbol_table
    coord
    speaker_pos
    wall
    room_properties
    configuration
    configuration_file
    room
  ].each { |f| require_relative File.join('crb', f) }
end
