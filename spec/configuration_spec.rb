# frozen_string_literal: true

require 'spec_helper'

RSpec.describe CRB::Configuration do

  before do
    good_conf_fh = File.open(File.join(FIXTURE_PATH, 'good.yml'), 'r')
    @good_conf = YAML.load(good_conf_fh)
    @tag = 'octo'
    @bad_args = [ 23, [ 23, 23 ], 'whatever' ]
    @bad_confs = [ { 'bad_conf': { 'not_speaker': 23, 'room': [ 23, 23 ] }}, { 'another': { 'speakers': [[ 23, 23 ]], 'not_room': 23 }} ]
  end

  it 'can be built' do
    expect(CRB::Configuration.new(@good_conf)).not_to be nil
  end

  it 'functions properly' do
    gcs = @good_conf[@tag]
    gcs_full = { @tag => gcs }
    expect((c = CRB::Configuration.new(gcs_full))).not_to be nil
    spn = 0
    gcs['speakers'].each do
      |k, v|
      expect(c.speakers[k.to_s].x).to eq(v[0])
      expect(c.speakers[k.to_s].y).to eq(v[1])
      spn += 1
    end
    expect(c.room.width).to eq(gcs['room']['width'])
    expect(c.room.depth).to eq(gcs['room']['depth'])
  end

  it 'checks on arguments' do
    @bad_args.each do
      |bc|
      expect { CRB::Configuration.new(bc) }.to raise_error(CRB::Errors::ConfigArgumentError)
    end
  end

  it 'checks on configuration consistency' do
    @bad_confs.each do
      |bc|
      expect { CRB::Configuration.new(bc) }.to raise_error(CRB::Errors::BadConfigError)
    end
  end

end
