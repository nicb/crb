# frozen_string_literal: true

require 'spec_helper'
require 'yaml'

RSpec.describe CRB::ConfigurationFile do

  before do
    @good_conf = File.expand_path(File.join(FIXTURE_PATH, 'good.yml'), __FILE__)
    @bad_conf  = File.expand_path(File.join(FIXTURE_PATH, 'bad.yml'),  __FILE__)
    @unexistent_file = File.expand_path(File.join(FIXTURE_PATH, 'I_do_not_exist.yml'),  __FILE__)
  end

  it 'can be built' do
    expect(CRB::ConfigurationFile.new(@good_conf)).not_to be nil
  end

  it 'functions properly' do
    good_conf_size = YAML.load(File.open(@good_conf, 'r')).keys.size
    expect((c = CRB::ConfigurationFile.new(@good_conf))).not_to be nil
    expect(c.confs.size).to eq(good_conf_size)
  end

  it 'checks on arguments' do
    expect { CRB::ConfigurationFile.new(@unexistent_file) }.to raise_error(CRB::Errors::ConfigFileNotFound)
  end

  it 'checks on configuration consistency' do
    expect { CRB::ConfigurationFile.new(@bad_conf) }.to raise_error(CRB::Errors::Error)
  end

  it 'looks up for the proper configuration' do
    expect((c = CRB::ConfigurationFile.new(@good_conf))).not_to be nil
    full_conf = YAML.load(File.open(@good_conf, 'r'))
    full_conf.keys.each do
      |tag|
      expect(c.conf(tag).tag).to eq(tag)
    end
  end

end
