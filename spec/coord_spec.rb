# frozen_string_literal: true

require 'spec_helper'
require 'yaml'

RSpec.describe CRB::Coord do

  before do
    @good_conf = YAML.load(File.open(File.join(FIXTURE_PATH, 'good.yml'), 'r'))['octo'] 
    @good_pos_pars = @good_conf['speakers'][1]
    @bad_pars = [ 23, 'twenty-three', 23.23, [ 23, 23, 23 ] ]
  end

  it 'can be built' do
    expect(CRB::Coord.new(@good_pos_pars)).not_to be nil
  end

  it 'has functional methods' do
    expect((c = CRB::Coord.new(@good_pos_pars))).not_to be nil
    expect(c.respond_to?(:x)).to be true
    expect(c.respond_to?(:y)).to be true
    expect(c.x).to eq(@good_pos_pars[0])
    expect(c.y).to eq(@good_pos_pars[1])
  end

  it 'checks arguments' do
    @bad_pars.each do
      |args|
      expect { CRB::Coord.new(args) }.to raise_error(CRB::Errors::CoordArgumentError)
    end
  end

	describe CRB::SpeakerPos do

    before do
      spk_pars = @good_conf['speakers'].first
      @good_spk_pars = { spk_pars[0] => spk_pars[1] }
    end
	
	  it 'can be built' do
      expect(CRB::SpeakerPos.new(@good_spk_pars)).not_to be nil
	  end

    it 'sports a tag' do
      expect((s = CRB::SpeakerPos.new(@good_spk_pars))).not_to be nil
      expect(s.tag).to eq(@good_spk_pars.keys.first)
    end
	
	end
	
	describe CRB::RoomProperties do
	
    before do
      @good_room_pars = @good_conf['room']
    end

	  it 'can be built' do
	    expect(CRB::RoomProperties.new(@good_room_pars)).not_to be nil
	  end

    it 'has functional methods' do
      expect((c = CRB::RoomProperties.new(@good_room_pars))).not_to be nil
      expect(c.respond_to?(:width)).to be true
      expect(c.respond_to?(:depth)).to be true
      expect(c.width).to eq(@good_room_pars['width'])
      expect(c.depth).to eq(@good_room_pars['depth'])
    end
	
	end

end
