# frozen_string_literal: true

require 'spec_helper'

RSpec.describe CRB::Room do

  include CRB::ClassMethods

  before do
    conffile = File.join(FIXTURE_PATH, 'good.yml')
    cfile = CRB::ConfigurationFile.new(conffile)
    @conf = cfile.conf('octo')
  end

  it 'can be built' do
    expect(CRB::Room.new(@conf)).not_to be nil
  end

  it 'functions properly...' do
    expect((r = CRB::Room.new(@conf))).not_to be nil
    expect(r.run).to match(/instr 300, 301/)
    expect(r.run).to match(/ispkr[0-9][xy]/)
    File.open("room.orc", 'w') { |fh| fh.puts r.run }
    sym_log
  end

  it '... and also with the gravel configuration' do
    expect((r = CRB::Room.new(@conf, 'room_gravel.orc.erb'))).not_to be nil
    expect(r.run).to match(/instr 300, 301/)
    expect(r.run).to match(/ispkr[0-9][xy]/)
    File.open("room.orc", 'w') { |fh| fh.puts r.run }
    sym_log
  end

end
