# frozen_string_literal: true

require 'spec_helper'

RSpec.describe CRB::SymbolTable do

  include CRB::ClassMethods

  before do
    @key_1 = 'x'
    @terms = { @key_1 => 2, 'y' => 3, 'z' => 4, '23' => 1 }
  end

  after do
    CRB::SymbolTable.instance.clear
  end

  it 'cannot be built (because it is a singleton' do
    expect { CRB::SymbolTable.new(@good_conf) }.to raise_error(NoMethodError)
  end

  it 'has an s() method that works to add symbols' do
    st = CRB::SymbolTable.instance
    expect(s(@key_1)).to eq(@key_1)
    expect(st[@key_1]).to eq(1)
  end

  it 'has a report() method that works' do
    st = CRB::SymbolTable.instance
    @terms.each do
      |k, v|
      1.upto(v) { s(k) }
    end
    report = st.report
    expect(report).to match(/#{@key_1}: *used *2 times/)
    expect(report).to match(/y: *used *3 times/)
    expect(report).to match(/z: *used *4 times/)
    expect(report).to match(/23: *used *1 time/)
  end

end
