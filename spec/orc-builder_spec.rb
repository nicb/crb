# frozen_string_literal: true

require 'spec_helper'

RSpec.describe CRB do
  it "has a version number" do
    expect(CRB::VERSION).not_to be nil
  end
end
