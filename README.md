# `csound` room orchestra builder (written in `ruby`)

Given a configuration file, `crb` creates the appropriate sound spatialization orchestra for `csound`.

## Configuration file

The configuration file works as follows:

```yaml
stereo:
    speakers:
        - left: -5,5
        - right: 5,5
    room:
        - width: 20
        - depth: 30
    num_instr:   50

quad:
    ...
```
